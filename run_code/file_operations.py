from cryptology.shift import *
from cryptology.affine import *
from cryptology.vigenere import *
from cryptology.rsa import *


def encipher_shift_file_operation(source, key):
    enciphered_message = []
    stream = open(source, "r")
    for word in stream.readlines():
        enciphered_word = shift_encipher(word, list(key))                      # one word is enciphered by letters
        enciphered_message.append(enciphered_word)
    stream.close()
    stream = open(source, "+w")
    for enciphered_word in enciphered_message:
        for enciphered_char in enciphered_word:
            stream.write(enciphered_char)
    stream.close()


def decipher_shift_file_operation(source, key):
    deciphered_message = []
    stream = open(source, "r")
    for word in stream.readlines():
        enciphered_word = shift_decipher(word, list(key))
        deciphered_message.append(enciphered_word)
    stream.close()
    stream = open(source, "+w")
    for enciphered_word in deciphered_message:
        for enciphered_char in enciphered_word:
            stream.write(enciphered_char)
    stream.close()


def encipher_affine_file_operation(source, key):
    enciphered_message = []
    stream = open(source, "r")
    for word in stream.readlines():
        enciphered_word = affine_encipher(word, list(key))
        enciphered_message.append(enciphered_word)
    stream.close()
    stream = open(source, "+w")
    for enciphered_word in enciphered_message:
        for enciphered_char in enciphered_word:
            stream.write(enciphered_char)
    stream.close()


def decipher_affine_file_operation(source, key):
    enciphered_message = []
    stream = open(source, "r")
    for word in stream.readlines():
        enciphered_word = affine_decipher(word, list(key))
        enciphered_message.append(enciphered_word)
    stream.close()
    stream = open(source, "+w")
    for enciphered_word in enciphered_message:
        for enciphered_char in enciphered_word:
            stream.write(enciphered_char)
    stream.close()


def encipher_vigenere_file_operation(source, key):
    enciphered_message = []
    stream = open(source, "r")
    for word in stream.readlines():
        enciphered_word = vigenere_encipher(word, list(key))
        enciphered_message.append(enciphered_word)
    stream.close()
    stream = open(source, "+w")
    for enciphered_word in enciphered_message:
        for enciphered_char in enciphered_word:
            stream.write(enciphered_char)
    stream.close()


def decipher_vigenere_file_operation(source, key):
    enciphered_message = []
    stream = open(source, "r")
    for word in stream.readlines():
        enciphered_word = vigenere_decipher(word, list(key))
        enciphered_message.append(enciphered_word)
    stream.close()
    stream = open(source, "+w")
    for enciphered_word in enciphered_message:
        for enciphered_char in enciphered_word:
            stream.write(enciphered_char)
    stream.close()


def encipher_rsa_file_operation(source, key):
    enciphered_message = []
    stream = open(source, "r")
    for word in stream.readlines():
        enciphered_word = rsa_encipher(word, list(key))
        enciphered_message.append(enciphered_word)
    stream.close()
    stream = open(source, "+w")
    for enciphered_word in enciphered_message:
        for enciphered_char in enciphered_word:
            stream.write(str(enciphered_char))
    stream.close()


def decipher_rsa_file_operation(source, key):
    deciphered_message = []
    stream = open(source, "r")
    for word in stream.readlines():
        enciphered_word = rsa_decipher(word, list(key))
        deciphered_message.append(enciphered_word)
    stream.close()
    stream = open(source, "+w")
    for enciphered_word in deciphered_message:
        for enciphered_char in enciphered_word:
            stream.write(str(enciphered_char))
    stream.close()
