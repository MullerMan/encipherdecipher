import sys


def shift_error_handling(source: str, key: list):
    try:
        opening_file = open(source, "r")                                       # working address
        opening_file.close()
        if not key[0].isdigit():                                               # only digit
            raise ValueError
        if len(sys.argv) > 4:                                                  # one key-argument
            raise ValueError
    except FileNotFoundError:
        print("Wrong directory (source)")
        sys.exit(0)
    except ValueError:
        print("Wrong key. Need one argument (int).")
        sys.exit(0)


def affine_error_handling(source: str, key: list):
    try:
        opening_file = open(source, "r")                                       # working address
        opening_file.close()
        if not key[0].isdigit() or not key[1].isdigit():                       # keys have to be digits
            raise ValueError
        if not int(key[0]) % 2:                                                # first key-argument has to be even
            raise ValueError
        if len(sys.argv) > 5:                                                  # two key-arguments
            raise ValueError
    except FileNotFoundError:
        print("Wrong directory (source)")
        sys.exit(0)
    except ValueError:
        print("Wrong key. Need two arguments (int, int). First has to be even.")
        sys.exit(0)


def vigenere_error_handling(source: str, key: list):
    try:
        opening_file = open(source, "r")                                       # working address
        opening_file.close()
        if not key[0].isalpha():                                               # only word (letters)
            raise ValueError
        if len(sys.argv) > 4:                                                  # one key-argument
            raise ValueError
    except FileNotFoundError:
        print("Wrong directory (source)")
        sys.exit(0)
    except ValueError:
        print("Wrong key. Need one argument (str).")
        sys.exit(0)


def rsa_error_handling(source: str, key: list):
    try:
        opening_file = open(source, "r")                                       # working address
        opening_file.close()
        if len(sys.argv) > 6 or len(sys.argv) < 3:
            raise ValueError
        if len(key) != 3:                                                      # three key-arguments
            raise ValueError
        if not key[0].isdigit() or not key[1].isdigit() or not key[2].isdigit():         # keys have to be digits
            raise ValueError
    except FileNotFoundError:
        print("Wrong directory (source)")
        sys.exit(0)
    except ValueError:
        print("Wrong key. Need three arguments (int, int, int). Every num has to be prime number")
        sys.exit(0)
