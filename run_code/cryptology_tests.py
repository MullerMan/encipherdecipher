import unittest

from cryptology.shift import shift_encipher, shift_decipher
from cryptology.affine import affine_encipher, affine_decipher
from cryptology.vigenere import vigenere_encipher, vigenere_decipher
from cryptology.rsa import rsa_encipher, rsa_decipher


class MainTests(unittest.TestCase):

    def setUp(self) -> None:
        self.basic_word = "ABC"
        self.basic_word_white_signs = "A B\nC"
        self.basic_num = "123"
        self.basic_num_white_signs = "1 2\n3"

        self.sh_key = ["3"]                                                    # shift_key
        self.enciphered_shift_word = ['D', 'E', 'F']
        self.deciphered_shift_word = ['X', 'Y', 'Z']
        self.enciphered_shift_word_signs = ['D', ' ', 'E', '\n', 'F']
        self.deciphered_shift_word_signs = ['X', ' ', 'Y', '\n', 'Z']

        self.af_key = ["3", "5"]                                               # affine_key
        self.enciphered_affine_word = ['F', 'I', 'L']
        self.deciphered_affine_word = ['H', 'Q', 'Z']
        self.enciphered_affine_word_signs = ['F', ' ', 'I', '\n', 'L']
        self.deciphered_affine_word_signs = ['H', ' ', 'Q', '\n', 'Z']

        self.vi_key = ["hello"]                                                # vigenere_key
        self.enciphered_vigenere_word = ['H', 'I', 'J']
        self.deciphered_vigenere_word = ['T', 'U', 'V']
        self.enciphered_vigenere_word_signs = ['H', ' ', 'I', '\n', 'J']
        self.deciphered_vigenere_word_signs = ['T', ' ', 'U', '\n', 'V']

        self.rsa_key = ["3", "5", "7"]                                         # rsa_key
        self.enciphered_rsa_num = [1, 8, 12]
        self.deciphered_rsa_num = [1, 8, 12]
        self.enciphered_rsa_num_signs = [1, ' ', 8, '\n', 12]
        self.deciphered_rsa_num_signs = [1, ' ', 8, '\n', 12]

    def test_shift_encipher(self):
        self.assertEqual(self.enciphered_shift_word, shift_encipher(self.basic_word, self.sh_key))

    def test_shift_encipher_with_white_signs(self):
        self.assertEqual(self.enciphered_shift_word_signs, shift_encipher(self.basic_word_white_signs, self.sh_key))

    def test_shift_decipher(self):
        self.assertEqual(self.deciphered_shift_word, shift_decipher(self.basic_word, self.sh_key))

    def test_shift_decipher_with_white_signs(self):
        self.assertEqual(self.deciphered_shift_word_signs, shift_decipher(self.basic_word_white_signs, self.sh_key))

    def test_affine_encipher(self):
        self.assertEqual(self.enciphered_affine_word, affine_encipher(self.basic_word, self.af_key))

    def test_affine_encipher_with_white_signs(self):
        self.assertEqual(self.enciphered_affine_word_signs, affine_encipher(self.basic_word_white_signs, self.af_key))

    def test_affine_decipher(self):
        self.assertEqual(self.deciphered_affine_word, affine_decipher(self.basic_word, self.af_key))

    def test_affine_decipher_with_signs(self):
        self.assertEqual(self.deciphered_affine_word_signs, affine_decipher(self.basic_word_white_signs, self.af_key))

    def test_vigenere_encipher(self):
        self.assertEqual(self.enciphered_vigenere_word, vigenere_encipher(self.basic_word, self.vi_key))

    def test_vigenere_encipher_with_white_signs(self):
        self.assertEqual(self.enciphered_vigenere_word_signs, vigenere_encipher(self.basic_word_white_signs, self.vi_key))

    def test_vigenere_decipher(self):
        self.assertEqual(self.deciphered_vigenere_word, vigenere_decipher(self.basic_word, self.vi_key))

    def test_vigenere_decipher_with_white_signs(self):
        self.assertEqual(self.deciphered_vigenere_word_signs, vigenere_decipher(self.basic_word_white_signs, self.vi_key))

    def test_rsa_encipher(self):
        self.assertEqual(self.enciphered_rsa_num, rsa_encipher(self.basic_num, self.rsa_key))

    def test_rsa_encipher_with_white_signs(self):
        self.assertEqual(self.deciphered_rsa_num_signs, rsa_decipher(self.basic_num_white_signs, self.rsa_key))

    def test_rsa_decipher(self):
        self.assertEqual(self.deciphered_rsa_num, rsa_decipher(self.basic_num, self.rsa_key))

    def test_rsa_decipher_with_white_signs(self):
        self.assertEqual(self.deciphered_rsa_num_signs, rsa_decipher(self.basic_num_white_signs, self.rsa_key))


if __name__ == '__main__':
    unittest.main()
