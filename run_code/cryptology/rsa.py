from sympy.crypto.crypto import rsa_public_key, rsa_private_key, encipher_rsa, decipher_rsa


def rsa_encipher(string: str, key: list):
    public_key = rsa_public_key(int(key[0]), int(key[1]), int(key[2]))
    enciphered_num = []
    for digit in string:
        if digit.isdigit():                                                    # enciphering only digits
            enciphered_digit = encipher_rsa(int(digit), public_key)
            enciphered_num.append(enciphered_digit)
        else:                                                                  # letters, white signs
            enciphered_num.append(digit)
    return enciphered_num                                                      # return list with enciphered digits


def rsa_decipher(number: str, key: list):
    private_key = rsa_private_key(int(key[0]), int(key[1]), int(key[2]))
    deciphered_num = []
    for digit in number:
        if digit.isdigit():                                                    # enciphering only digits
            enciphered_digit = decipher_rsa(int(digit), private_key)
            deciphered_num.append(enciphered_digit)
        else:                                                                  # letters, white signs
            deciphered_num.append(digit)
    return deciphered_num                                                      # return list with enciphered digits
