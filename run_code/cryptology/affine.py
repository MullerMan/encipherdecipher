from sympy.crypto.crypto import encipher_affine, decipher_affine


def affine_encipher(string: str, key: list):
    enciphered_word = []
    for char in string:
        if char.isalpha():                                                     # enciphering only english alphabet
            enciphered_char = encipher_affine(char, (int(key[0]), int(key[1])))
            enciphered_word.append(enciphered_char)
        else:                                                                  # digits, white signs
            enciphered_word.append(char)
    return enciphered_word                                                     # return list with enciphered letters


def affine_decipher(string: str, key: list):
    deciphered_word = []
    for char in string:
        if char.isalpha():                                                     # deciphering only english alphabet
            enciphered_char = decipher_affine(char, (int(key[0]), int(key[1])))
            deciphered_word.append(enciphered_char)
        else:                                                                  # digits, white signs
            deciphered_word.append(char)
    return deciphered_word                                                     # return list with deciphered letters
