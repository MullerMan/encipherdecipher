import argparse
import textwrap


def parse_args(args):
    parser = argparse.ArgumentParser(prog="Ciphering program", usage="enciphering and deciphering files",
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent("""
                                                Options:            Description:                    Type of key:
                                                shift_encipher     - enciphering in shift mode      (int)
                                                shift_decipher     - deciphering in shift mode      (int)
                                                affine_encipher    - enciphering in affine mode     (even int, int)
                                                affine_decipher    - deciphering in affine mode     (even int, int)
                                                vigenere_encipher  - enciphering in vigenere mode   (str)
                                                vigenere_decipher  - deciphering in vigenere mode   (str)
                                                rsa_encipher       - enciphering in rsa mode        (pi, pi, pi) pi -> prime int
                                                rsa_decipher       - deciphering in rsa mode        (pi, pi, pi) pi -> prime int
                                                """))
    parser.add_argument("option", help="Choice of the mode", type=str)                             # str
    parser.add_argument("source", help="Source of file to encipher or decipher", type=str)         # str
    parser.add_argument("key", nargs="+", help="Special key to encipher or decipher chosen file")  # list
    return parser.parse_args(args)
